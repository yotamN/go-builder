#!/usr/bin/env python3

# go.py
#
# Copyright 2018 Yotam Nachum <yotamnachum1@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import shutil
import threading
from typing import List
from subprocess import call

import gi
gi.require_version('Ide', '1.0')
from gi.repository import (
    GObject,
    Ide,
    Gio,
    GLib
)

SEVERITY_MAP = {
    'warning': Ide.DiagnosticSeverity.WARNING,
    'error': Ide.DiagnosticSeverity.ERROR
}

LINTER_OUTPUT_FORMAT = '{{.Line}}:{{if .Col}}{{.Col}}{{end}}:{{.Severity}}: {{.Message}} ({{.Linter}})'
LINTER_COLUMN_SEPERATOR = ':'
LINTER_LINE_INDEX = 0
LINTER_COLUMN_INDEX = 1
LINTER_SEVERITY_INDEX = 2
LINTER_ERROR_MESSAGE_INDEX = 3

DEFAULT_DIAGNOSTIC_LINE = 0
DEFAULT_DIAGNOSTIC_COLUMN = 0


class GoLintDiagnosticProvider(Ide.Object, Ide.DiagnosticProvider):
    """
    check Go files for diagnostic errors using the golint tool
    """

    @staticmethod
    def _get_linter() -> str:
        """
        make sure gometalinter is installed and find the path
        """

        if shutil.which('gometalinter.v2') is None:
            call(['go', 'get', '-u', 'gopkg.in/alecthomas/gometalinter.v2'])

        return shutil.which('gometalinter.v2')

    @staticmethod
    def _get_launcher(pipeline: Ide.BuildPipeline) -> Ide.SubprocessLauncher:
        """
        get a launcher object to run the Go lint executable
        """

        srcdir = pipeline.get_srcdir()
        runtime = pipeline.get_configuration().get_runtime()
        launcher = runtime.create_launcher()
        launcher.set_flags(Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE)
        launcher.set_cwd(srcdir)

        return launcher

    def do_diagnose_async(self, file: Ide.File, buffer: Ide.Buffer, cancellable, callback, user_data):
        """
        run the golint diagnostic tool and show warnings in the editor
        """

        self.diagnostics_list = []
        task = Gio.Task.new(self, cancellable, callback)
        task.diagnostics_list = []

        context = self.get_context()
        unsaved_file = context.get_unsaved_files().get_unsaved_file(file.get_file())
        pipeline = self.get_context().get_build_manager().get_pipeline()
        srcdir = pipeline.get_srcdir()
        launcher = self._get_launcher(pipeline)

        if unsaved_file:
            file_content = unsaved_file.get_content().get_data().decode('utf-8')
        else:
            file_content = None

        threading.Thread(target=self.execute, args=(task, launcher, srcdir, file, file_content),
                         name='golint-thread').start()

    def execute(self, task: Gio.Task, launcher: Ide.SubprocessLauncher, srcdir: str, file: Ide.File, file_content: str):
        try:
            launcher.push_args((self._get_linter(), '--format', LINTER_OUTPUT_FORMAT, file.get_path()))

            sub_process = launcher.spawn()
            success, stdout, _ = sub_process.communicate_utf8(file_content, None)

            if not success:
                task.return_boolean(False)
                return

            diagnostics = self._parse_diagnostic(file, stdout)
            task.diagnostics_list = diagnostics
        except GLib.Error as err:
            task.return_error(err)
        else:
            task.return_boolean(True)

    @staticmethod
    def _parse_diagnostic(file: Ide.File, stdout: str) -> Ide.Diagnostic:
        """
        parse go meta linter stdout and return a list of diagnostics
        """

        diagnostics = []

        lines = stdout.split('\n')
        for line in lines:
            line_columns = line.split(LINTER_COLUMN_SEPERATOR)

            try:
                # lines start at 0 in Idelib
                line = int(line_columns[LINTER_LINE_INDEX]) - 1
                column = int(line_columns[LINTER_COLUMN_INDEX]) - 1
                start = Ide.SourceLocation.new(file, line, column, 0)

                severity = SEVERITY_MAP.get(line_columns[LINTER_SEVERITY_INDEX], Ide.DiagnosticSeverity.WARNING)
                error_message = LINTER_COLUMN_SEPERATOR.join(line_columns[LINTER_ERROR_MESSAGE_INDEX:])
            except ValueError:
                line = DEFAULT_DIAGNOSTIC_LINE
                column = DEFAULT_DIAGNOSTIC_COLUMN
            else:
                diagnostic = Ide.Diagnostic.new(severity, error_message, start)
                diagnostics.append(diagnostic)

        return diagnostics


    def do_diagnose_finish(self, result: Gio.Task) -> Ide.Diagnostics:
        """
        return the diagnostic task when finished
        """

        if result.propagate_boolean():
            return Ide.Diagnostics.new(result.diagnostics_list)


class GoBuildSystem(Ide.Object, Ide.BuildSystem, Gio.AsyncInitable):
    project_file = GObject.Property(type=Gio.File)

    def do_get_id(self) -> str:
        return 'golang'

    def do_get_display_name(self) -> str:
        return 'Go'

    def do_init_async(self, io_priority, cancellable, callback, data):
        task = Gio.Task.new(self, cancellable, callback)

        try:
            if self.props.project_file.get_basename() in ('go.mod',):
                task.return_boolean(True)
                return

            if self.props.project_file.query_file_type(0) == Gio.FileType.DIRECTORY:
                child = self.props.project_file.get_child('go.mod')
                if child.query_exists(None):
                    self.props.project_file = child
                    task.return_boolean(True)
                    return
        except Exception as ex:
            task.return_error(ex)

        task.return_error(Ide.NotSupportedError())

    def do_init_finish(self, task: Gio.Task):
        return task.propagate_boolean()

    def do_get_priority(self) -> int:
        return 500


class GoPipelineAddin(Ide.Object, Ide.BuildPipelineAddin):
    """
    The GoPipelineAddin is responsible for creating the necessary build
    stages and attaching them to phases of the build pipeline.
    """

    def do_load(self, pipeline: Ide.BuildPipeline):
        context = self.get_context()
        build_system = context.get_build_system()

        # Ignore pipeline unless this is a Go project
        if not isinstance(build_system, GoBuildSystem):
            return

        project_file = build_system.props.project_file
        config = pipeline.get_configuration()
        builddir = pipeline.get_builddir()
        runtime = config.get_runtime()

        if not runtime.contains_program_in_path('go'):
            raise OSError('The runtime must contain go to build Go modules')

        fetch_launcher = pipeline.create_launcher()
        fetch_launcher.set_cwd(project_file.get_parent().get_path())
        fetch_launcher.push_argv('go')
        fetch_launcher.push_argv('mod')
        fetch_launcher.push_argv('download')

        # stage = Ide.BuildStageLauncher.new(context, fetch_launcher)
        # stage.set_name(_("Building Dependencies"))
        # self.track(pipeline.connect(Ide.BuildPhase.DEPENDENCIES, 0, stage))

        prefix = config.get_prefix()
        project_name = project_file.get_parent().get_basename()

        fetch_launcher = pipeline.create_launcher()
        fetch_launcher.set_cwd(project_file.get_parent().get_path())
        fetch_launcher.push_argv('go')
        fetch_launcher.push_argv('build')
        # fetch_launcher.push_argv('-o')
        # fetch_launcher.push_argv(prefix + '/bin/' + project_name)

        stage = Ide.BuildStageLauncher.new(context, fetch_launcher)
        stage.set_name(_("Building The Module"))
        self.track(pipeline.connect(Ide.BuildPhase.BUILD, 0, stage))


class GoBuildTarget(Ide.Object, Ide.BuildTarget):

    def do_get_install_directory(self):
        context = self.get_context()
        project_file = context.get_project_file()
        cwd = project_file.get_parent()
        return cwd

    def do_get_name(self) -> str:
        """
        return the name of the created executable file_content
        """

        context = self.get_context()
        project_name = context.get_project_file().get_parent().get_basename()
        return project_name

    def do_get_language(self) -> str:
        return 'go'

    def do_get_cwd(self) -> str:
        context = self.get_context()
        project_file = context.get_project_file()
        cwd = project_file.get_parent().get_path()
        return cwd

    def do_get_argv(self) -> List[str]:
        return []

    def do_get_priority(self) -> int:
        """
        return the priority of the BuildTarget
        """

        return 500


class GoBuildTargetProvider(Ide.Object, Ide.BuildTargetProvider):

    def do_get_targets_async(self, cancellable: Gio.Cancellable, callback, data):
        task = Gio.Task.new(self, cancellable, callback)
        task.set_priority(GLib.PRIORITY_LOW)

        context = self.get_context()
        build_system = context.get_build_system()

        if not isinstance(build_system, GoBuildSystem):
            task.return_error(GLib.Error('Not go build system',
                                         domain=GLib.quark_to_string(Gio.io_error_quark()),
                                         code=Gio.IOErrorEnum.NOT_SUPPORTED))
            return

        task.targets = [GoBuildTarget(context=self.get_context())]
        task.return_boolean(True)

    def do_get_targets_finish(self, result: Gio.Task):
        if result.propagate_boolean():
            return result.targets



class GoPreferencesAddin(GObject.Object, Ide.PreferencesAddin):
    """
    the Go plugin preferences manager
    """

    def do_load(self, preferences):
        """
        load the go plugin preferences

        :param Ide.PreferencesPerspective preferences: the preferences object that hold all the plugins preferences
        """

        self.golint = preferences.add_switch("code-insight",
                                             "diagnostics",
                                             "org.gnome.builder.plugins.go",
                                             "enable-golint",
                                             None,
                                             "true",
                                             _("Go lint"),
                                             _("Enable the use of go meta linter, which run many diagnostic tools on your code"),
                                             # translators: these are keywords used to search for preferences
                                             _("go golint lint linter"),
                                             500)

    def do_unload(self, preferences):
        """
        unload all the go plugin preferences

        :param Ide.PreferencesPerspective preferences: the preferences object that hold all the plugins preferences
        """

        preferences.remove_id(self.golint)
